FROM debian:jessie
RUN apt-get update && apt-get install -y apache2 \
										 php5
COPY common.conf /etc/apache2/sites-available
COPY entry.sh /entry.sh
RUN a2dissite 000-default && a2ensite common
VOLUME ["/var/www", "/var/log/apache2", "/etc/apache2"]
ENTRYPOINT ["/entry.sh"]
